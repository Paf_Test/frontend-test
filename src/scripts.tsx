import React, { useState, useEffect } from "react";
import { createRoot } from "react-dom/client";

const Header: React.FC = () => {
    return <header>Paf Frontend Test</header>;
};

const Footer: React.FC = () => {
    return <footer>Designed by Bao 🤟</footer>;
};

//#region SEARCH COMPONENT

// Create a search component that allows the user to search for games.
const SearchComponent = ({
    onSearch,
}: {
    onSearch: (value: string) => void;
}) => {
    const [input, setInput] = useState("");
    const [searchHistory, setSearchHistory] = useState<string[]>([]);

    useEffect(() => {
        const history = localStorage.getItem("searchHistory");
        if (history) { 
            setSearchHistory(JSON.parse(history)); // Transform history string to array
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("searchHistory", JSON.stringify(searchHistory));
    }, [searchHistory]); // Update local storage when search history changes

    const [isDropdownVisible, setIsDropdownVisible] = useState(false);

    // Display letters as typing in input field
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput(event.target.value);  // Set input value to state
        onSearch(event.target.value);  // Pass search term to parent component
    };

    // Save search history to local storage, reused for Enter key and blur event
    const saveSearchHistory = (searchTerm: string) => {
        if (searchTerm) {
            const newHistory = [
                searchTerm,
                ...searchHistory.filter((item) => item !== searchTerm).slice(0, 9),
            ];
            setSearchHistory(newHistory);
            localStorage.setItem("searchHistory", JSON.stringify(newHistory));
        }
    };

    // Handle Enter key to search
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            saveSearchHistory(input);
            onSearch(input);
            setInput(""); // Clear the input field after search
        }
    };


    // Handle blur event to hide dropdown, save search history
    const handleInputBlur = () => {
        saveSearchHistory(input);
        setTimeout(() => setIsDropdownVisible(false), 300);
    };

    // Handle click on history item, hide dropdown, pass search term to input field, and search
    const handleHistoryItemClick = (searchTerm: string) => {
        setInput(searchTerm);
        onSearch(searchTerm);
        setTimeout(() => setIsDropdownVisible(false), 300); // Trick to hide dropdown after click 300ms
    };

    // Render search component
    return (
        <div className="search-container">
            <input
                className="search-input"
                type="text"
                placeholder="Search games..."
                value={input}
                onChange={handleInputChange}
                onKeyDown={handleKeyDown}
                onFocus={() => setIsDropdownVisible(true)}
                onBlur={handleInputBlur}
                autoComplete="off"
            />
            {isDropdownVisible && (
                <div className="search-history">
                    {searchHistory.map((search, index) => (
                        <div key={index} className="search-history-item" onClick={() => handleHistoryItemClick(search)}>
                            {search}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};
//#endregion

//#region GAME LIST COMPONENT

// Argument of lists.json file.
interface DataType {
    title: string;
    description: string;
    lists: Array<{
        id: string;
        title: string;
        items: Array<{
            id: string;
            image: string;
            title: string;
            provider: string;
        }>;
    }>;
}

const GameList: React.FC<{ searchTerm: string }> = ({ searchTerm }) => {
    const [data, setData] = useState<DataType | null>(null);
    const [filteredData, setFilteredData] = useState<DataType | null>(null);

    // Fetch data from lists.json
    useEffect(() => {
        fetch("/api/games/lists.json")
            .then((response) => response.json())
            .then((data) => {
                setData(data);
                setFilteredData(data);
            });
    }, []);

    // Filter data by search term
    useEffect(() => {
        if (!data) return; // Return if data is not ready

        if (searchTerm === "") { // If search term is empty, show all data
            setFilteredData(data);
        } else {
            const filteredLists = data.lists.map((list) => { // Filter
                return {
                    ...list,
                    items: list.items.filter((item) => 
                        item.title.toLowerCase().includes(searchTerm.toLowerCase()), // Case insensitive
                    ),
                };
            });

            setFilteredData({ ...data, lists: filteredLists }); // Update filtered data
        }
    }, [searchTerm, data]); // Update when search term or data changes

    if (!filteredData) return <div>Loading...</div>;

    // Render game list component
    return (
        <div>
            <h1>{filteredData.title}</h1>
            <hr />
            <p>{filteredData.description}</p>
            {filteredData.lists.map((list) => (
                <div key={list.id}>
                    <h2>{list.title}</h2>
                    <div className={`game-grid ${list.items.length === 1 ? 'center-single-item' : ''}`}>
                        {list.items.map((game) => (
                            <div key={game.id} className="game-item">
                                <img src={game.image} alt={game.title} />
                                <p>{game.title}</p>
                            </div>
                        ))}
                    </div>
                </div>
            ))}
        </div>
    );
};
//#endregion

const MainComponent: React.FC = () => {
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <div>
            <Header />
            <SearchComponent onSearch={setSearchTerm} />
            <GameList searchTerm={searchTerm} />
            <Footer />
        </div>
    );
};

const root = createRoot(document.getElementById("root") as HTMLElement);
root.render(<MainComponent />);
